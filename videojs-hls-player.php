<?php
  /*
    Plugin Name: VideoJs HLS Player By Ruk-Com
    Description: videojs hls player
    Version: 1.5
    Author: Ruk-Com Co.,Ltd
    Author URI: https://hostings.ruk-com.in.th/
  */
  // plugin update checker
  require('vendor/plugin-update-checker/plugin-update-checker.php');
  $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/rukcom-software/videojs-hls-player/',
    __FILE__,
    'videojs-hls-player'
  );
  
  /* start adding script and style for videojs */
  add_action('wp_enqueue_scripts', 'register_rukcom_style');
  function register_rukcom_style() {
    wp_enqueue_style( 'ruk-com-cache-style', 'https://vjs.zencdn.net/6.6.3/video-js.css'); // Inside a plugin
    wp_enqueue_style( 'ruk-com-cache-bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css'); // Inside a plugin
    wp_enqueue_style( 'ruk-com-cache-style-2', plugins_url( '/css/style.css', __FILE__ )); // Inside a plugin
  }

  add_action( 'wp_footer', 'register_rukcom_script' );
  function register_rukcom_script() {
    global $meta_info;
    if (is_single()) {
      $video_ids = array_column($meta_info, "id");
  ?>  
      <script src="https://unpkg.com/video.js/dist/video.js"></script>
      <script src="https://unpkg.com/videojs-flash/dist/videojs-flash.js"></script>
      <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

      <script type="text/javascript">
        jQuery(document).ready(function(){
          var video_ids = <?php echo json_encode($video_ids); ?>;
          var player = [];
          jQuery.each(video_ids, function(key, value){
            if(jQuery("#videojs-hls-player-"+value).length) {
              player.push(videojs('videojs-hls-player-'+value, { "autoplay": false}));
            }
          });
        });
      </script>
  <?php
    }
  }
  /* end adding script and style for videojs */

  /* start adding meta box */
  $meta_info = array(
    array(
      'id' => "vod_m3u8",
      'name' => 'm3u8 link (THAI)',
      'desc' => 'Ex : https://drive1.cdn-mastermovie-hd.com/hls/content/drive-a-001/mp4/Annabelle2014.mp4/index.m3u8',
      'heading' => '***HD Master/เสียงไทย (T)***',
      'input_type' => 'text',
    ),
    array(
      'id' => "vod_m3u8_sub",
      'name' => 'm3u8 link (SUBTHAI)',
      'desc' => 'Ex : https://drive1.cdn-mastermovie-hd.com/hls/content/drive-a-001/mp4/Annabelle2014.mp4/index.m3u8',
      'heading' => '***HD Master/soundtrack (แปลไทย)***',
      'input_type' => 'text',
    ),
    array(
      'id' => "vod_m3u8_480",
      'name' => 'm3u8 link (480p)',
      'desc' => 'Ex : https://drive1.cdn-mastermovie-hd.com/hls/content/drive-a-001/mp4/Annabelle2014.mp4/index.m3u8',
      'heading' => '***HD Master (480p)***',
      'input_type' => 'text',
    ),
    array(
      'id' => "vod_m3u8_720",
      'name' => 'm3u8 link (720p)',
      'desc' => 'Ex : https://drive1.cdn-mastermovie-hd.com/hls/content/drive-a-001/mp4/Annabelle2014.mp4/index.m3u8',
      'heading' => '***HD Master (720p)***',
      'input_type' => 'text',
    ),
    array(
      'id' => "vod_m3u8_1080",
      'name' => 'm3u8 link (1080p)',
      'desc' => 'Ex : https://drive1.cdn-mastermovie-hd.com/hls/content/drive-a-001/mp4/Annabelle2014.mp4/index.m3u8',
      'heading' => '***HD Master (1080p)***',
      'input_type' => 'text',
    )
  );

  function videojs_display_markup($object) {
    global $meta_info, $post;
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
  ?>
    <div>
      <?php 
        foreach ($meta_info as $value) {
          $meta = get_post_meta($post->ID, $value['id'] , true);
      ?>  
          <label for="<?php echo $value['id']; ?>"><?php echo $value["name"] ?></label>
      <?php
          if ($value["input_type"] == "text") {
      ?>
            <input name="<?php echo $value['id']; ?>" type="text" value="<?php echo $meta; ?>" style="width:97%;">
      <?php
          } elseif ($value["input_type"] == "textarea") {
      ?>
            <textarea name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" cols="60" rows="4" style="width:97%"><?php echo $meta; ?></textarea>
      <?php
          }
      ?> 
          <p><?php echo $value["desc"]; ?></p><br/>
      <?php
        }
      ?>
    </div>
  <?php
  }

  function add_videojs_url_metabox() {
    add_meta_box("videojs-setting", "VideoJs Setting", "videojs_display_markup", "post", "normal", "high", null);
  }

  add_action("add_meta_boxes", "add_videojs_url_metabox");

  /* end adding meta box */

  /* start saving meta box value */
  function save_videojs_url_metabox($post_id, $post, $update)
  {
    global $meta_info;
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "post";
    if($slug != $post->post_type)
        return $post_id;

    foreach ($meta_info as $key => $value) {
      $current_value = "";
      if(isset($_POST[$value['id']]))
      {
        $current_value = $_POST[$value['id']];
      }   
      update_post_meta($post_id, $value['id'], $current_value);
    }
  }

  add_action("save_post", "save_videojs_url_metabox", 10, 3);
  /* end saving meta box value */

  /* start showing meta in single post */
  function rukcom_add_to_content( $content ) {    
    global $meta_info, $post;
    if( is_single() && $post->post_type == 'post') {
      
      $video_menu = '<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">';
      $video_html = '<div class="tab-content" id="nav-tabContent">';
      foreach ($meta_info as $key => $value) {
        $video_link = get_post_meta(get_the_ID(), $value["id"], true);
        if($video_link != "") {
          $is_active = $key == 0 ? "active" : "";
          $is_selected = $key == 0 ? "true" : "false";
          $video_menu .= '<li class="nav-item">
            <a class="nav-link '.$is_active.'" id="'.$value["id"].'-tab" data-toggle="pill" href="#'.$value["id"].'" role="tab" aria-controls="'.$value["id"].'" aria-selected="'.$is_selected.'">'.$value["heading"].'</a>
          </li>';
         
          $is_show = $key == 0 ? "in show active" : "";
          $video_html .= '<div class="tab-pane fade '.$is_show.'" id="'.$value["id"].'" role="tabpanel" aria-labelledby="'.$value["id"].'-tab">';
          $video_html .= '<div class="video-player-section">
              <video id="videojs-hls-player-'.$value["id"].'" class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9" controls>
                <source src='.$video_link.' type="video/mp4">
              </video>
            </div>';
          $video_html .= '</div>';  
        }
      }
      $video_menu .= "</ul>";
      $video_html .= "</div>";
      $video_html = $video_menu.$video_html;
    
      $pos = strpos($content, "<iframe");
      if ($pos !== false) {
        $content = substr($content, 0, $pos). "<br>" . $video_html ."<br>" . substr($content, $pos);
      } else {
        $content .= $video_html;
      }
    }
    return $content;
  }
  add_filter( 'the_content', 'rukcom_add_to_content', 8 );
  /* end showing meta in single post */
?>